<?php

/**
 * @file
 * Contains the USPS API Class.
 */

/**
 * The USPS API Class.
 */
class UspsApi {

  // Build response array to return.
  public $response = array();

  // Firm name maximum 38 characters.
  public $firmName;
  // Apt or Suite number,
  // maximum 38 characters.
  public $address1;
  // Street address, not PO box or apt #,
  // maximum 38 characters.
  public $address2;
  // City maximum 15 characters.
  public $city;
  // State maximum 2 characters.
  public $state;
  // Zip maximum 5 characters.
  public $zip5;

  // This xml query string.
  public $xmlString;
  public $xmlUrl;

  // Debug tools.
  public $debugTools;

  // Proxy settings.
  protected $proxyEnable;
  protected $proxyServer;
  protected $proxyPort;

  // Userid from variable table.
  private $userId;


  /**
   * Constructor.
   *
   * Set the applicable properties.
   *
   * @param array $data
   *    Property values.
   */
  public function __construct($data = array()) {
    // If $data is an array, set the properties.
    if (is_array($data)) {
      // Set the address fields during instantiation.
      if (count($data) > 0) {
        foreach ($data as $name => $value) {
          $this->$name = $value;
        }
      }

      // Set userid from variable table.
      $this->userId = variable_get('usps_api_id', NULL);
      // If not set, add to error array.
      if ($this->userId == NULL) {
        $this->response['error'] = TRUE;
        $this->response['error_description'] = 'API userId not set in the settings form.';
      }

      // Set proxy settings from variable table.
      $this->proxyEnable = variable_get('usps_api_curl_proxy_enable', FALSE);
      if ($this->proxyEnable == TRUE) {
        $this->proxyServer = variable_get('usps_api_curl_proxy_server', NULL);
        $this->proxyPort = variable_get('usps_api_curl_proxy_port', NULL);
      }

      // Set the debug tools.
      $this->debugTools = variable_get('usps_api_debug', 0);
    }
    // If $data is not an array, set error to true.
    else {
      $this->response['error'] = TRUE;
      $this->response['error_description'] = 'Object not instantiated correctly, parameter must be an array.';
    }
  }

  /**
   * CURL Method.
   *
   * Makes the API call using cURL.
   *
   * @param string $url
   *   The API url as a string.
   *
   * @return string
   *   The XML response document or return with error.
   */
  protected function curlRequest($url) {
    // Config and start cURL.
    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $url);
    curl_setopt($curl, CURLOPT_HEADER, FALSE);
    curl_setopt($curl, CURLOPT_HTTPGET, TRUE);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

    // Add proxy if configured.
    if ($this->proxyEnable === 'true') {
      curl_setopt($curl, CURLOPT_PROXY, $this->proxyServer . ':' . $this->proxyPort);
    }

    // cURL result.
    $result = curl_exec($curl);

    // Add debug XML response to returned array.
    if (is_string($this->debugTools['xml_response'])) {
      $this->response['debug_xml_response'] = $result;
    }

    // Get cURL info array.
    $curl_response = curl_getinfo($curl);

    // Add debug cURL Response to returned array.
    if (is_string($this->debugTools['curl_response'])) {
      $this->response['debug_curl_response'] = $curl_response;
    }

    // Check cURL http_code status for error.
    // If error exists, add to error array then return method.
    if ($curl_response['http_code'] != '200') {
      $this->response['error'] = TRUE;
      $this->response['curl_failed'] = TRUE;
      curl_close($curl);
      return $this->response;
    }
    // Return an XML response string.
    curl_close($curl);
    return $result;
  }

  /**
   * Address Standardization Verification API.
   *
   * This API can handle up to 5 addresses in one call.
   * This method initially only handles one address.
   *
   * @return array
   *   Array of address fields or return with error.
   */
  public function validateStandardAddress() {
    // Check if constructor returns errors.
    if (isset($this->response['error']) && $this->response['error'] == TRUE) {
      return $this->response;
    }

    // The xml url.
    $this->xmlUrl = 'http://production.shippingapis.com';
    $this->xmlUrl .= '/ShippingAPI.dll?API=Verify&XML=';

    // Build the api xml query string.
    $this->xmlString = '<AddressValidateRequest USERID="' . $this->userId . '">';
    $this->xmlString .= '<Address ID="0">';
    $this->xmlString .= '<FirmName>' . $this->firmName . '</FirmName>';
    $this->xmlString .= '<Address1>' . $this->address1 . '</Address1>';
    $this->xmlString .= '<Address2>' . $this->address2 . '</Address2>';
    $this->xmlString .= '<City>' . $this->city . '</City>';
    $this->xmlString .= '<State>' . $this->state . '</State>';
    $this->xmlString .= '<Zip5>' . $this->zip5 . '</Zip5>';
    $this->xmlString .= '<Zip4></Zip4>';
    $this->xmlString .= '</Address>';
    $this->xmlString .= '</AddressValidateRequest>';

    // Add debug Xml Request to returned array.
    if (is_string($this->debugTools['xml_request'])) {
      $this->response['debug_xml_request'] = $this->xmlUrl . $this->xmlString;
    }

    // Build xml query url.
    $xu = $this->xmlUrl . rawurlencode($this->xmlString);

    // Make cURL request.
    $result = $this->curlRequest($xu);

    // If curlRequest $result is a valid string.
    if (is_string($result)) {

      // Create an xml object.
      $x = new SimpleXmlElement($result);

      // Check for XML external entity injection.
      foreach ($x->childNodes as $child) {
        if ($child->nodeType === XML_DOCUMENT_TYPE_NODE) {
          $this->response['error'] = TRUE;
          $this->response['error_description'] = 'XML External Entity detected.';
          return $this->response;
        }
      }
    }
    else {
      // Return with cURL error.
      return $this->response;
    }

    // Check if error document response was returned.
    if ($x->Address->Error) {
      $this->response['error'] = TRUE;
      $this->response['number'] = (string) $x->Address->Error->Number;
      $this->response['source'] = (string) $x->Address->Error->Source;
      $this->response['description'] = (string) $x->Address->Error->Description;

      // Return with error array.
      return $this->response;
    }

    // Build and return valid address array.
    if ($x->Address->Firmname > 1) {
      $this->response['firmName'] = (string) $x->Address->FirmName;
    }
    if ($x->Address->Address1 > 1) {
      $this->response['address1'] = (string) $x->Address->Address1;
    }
    $this->response['address2'] = (string) $x->Address->Address2;
    $this->response['city'] = (string) $x->Address->City;
    $this->response['state'] = (string) $x->Address->State;
    $this->response['zip5'] = (string) $x->Address->Zip5;
    $this->response['zip4'] = (string) $x->Address->Zip4;

    return $this->response;
  }

}
